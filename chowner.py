import os, re, argparse, shutil, argparse

# set up a parser object
parser = argparse.ArgumentParser()
parser.add_argument('path', help="Quoted string that represents the path to root folder. Use only forward slashes(/), no trailing slash, no UNC paths.")
parser.add_argument('owner', help="The new text/owner. Can be username (with no quotes) or 'username1,username2'. ")
args = parser.parse_args()

# Enter the new text string to use for the owner
new_text = args.owner

# path to root folder. use only Forward slashes (/)
# no trailing slash; no UNC paths
path = os.path.abspath(args.path) 

# tuple that represents the extensions to check
extensions = ('.asp', '.html') 

#==============================================================================#
for root, dirs, files in os.walk(path):
    for file in files:

        if os.path.splitext(file)[1] in extensions:
            namein = os.path.join(root, file)
            nameout = os.path.join(root, file + '.tmp')

            fin = open(namein, 'rU')
            fout = open(nameout, 'w')

            print namein
            for line in fin.readlines(): 
                fout.write(re.sub(r'(<meta name="owner" content=").[^"]*(.*">)', '\\1' + new_text + '\\2', line))

            fin.close()
            fout.close()
            shutil.move(nameout, namein)
